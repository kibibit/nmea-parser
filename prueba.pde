import processing.serial.*;
Serial myPort;  // Create object from Serial class
int lf=10;

int i,j,k;
boolean v;
int secuencia[] = {1,2,4,7,9,15,16,17,3,9,1,3,7};
//String datos="";
String[] datos = new String[13];
String[] gps = new String[5];

String[] lineas;
//String linea1="$GPGGA,025641.000,0438.3963,N,07411.9671,W,1,7,1.06,2565.2,M,3.5,M,,*40";
String linea1;

void setup(){
  //lineas[0]="hola";
  //lineas[1]="mundo";
  //lineas = hola.split(",");
  //saveStrings("datos.txt",lineas);
  size(400,300);
  background(0);
  String portName = "/dev/ttyUSB0";
  myPort = new Serial(this, portName, 9600);
  myPort.bufferUntil(lf);
  textSize(12);
  //datos=null;
}
void draw(){
  background(0);
  //lineas = linea1.split(",");
  //println(linea1);
  //concatenar(); 
  mostrarDatos(); //<>//
}

void mostrarDatos(){
  for(int w=0;w<5;w++) {
    if(gps[w]!=null) text(gps[w],25,12*w+200);
    //else text("null",25,12*w+200);
  }
  for(int w=0;w<13;w++) {
    if(datos[w]!=null) text(datos[w],25,12*w+37);
    //else text("null",25,12*w+200);
  }
}
void concatenar(){
  v=true;
  switch(lineas[0]){
    case "$GPGGA":
      i=0;j=4;gps[0]=linea1;
      break;
    case "$GPGSA":
      i=5;j=7;gps[1]=linea1;
      break;
    case "$GPGSV":
      i=j=8;gps[2]=linea1;
      break;
    case "$GPRMC":
      i=j=9;gps[3]=linea1;
      break;
    case "$GPVTG":
      i=10;j=12;gps[4]=linea1;
      break;
    //case "$GPGLL":
    default:
      v=false;
      break;
  }
  if(v){
    for( ;i<j+1;i++){
      k=secuencia[i];
      datos[i]=lineas[k];
    }
  }
}

void serialEvent(Serial p) {
  linea1 = p.readString();
  lineas = linea1.split(",");
  concatenar();
}
