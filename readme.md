NMEA Parser
===========

Este es un script en processing que extrae los datos entrantes de un GPS quectel l80 para mostrarlos en pantalla.

El algoritmo de implementacion es una maquina de estados que extra los datos correspondientes a:

+ Hora(UTC)
+ Latitud
+ Longitud
+ Satelites usados para la geolocalizacion
+ Altitud
+ PDOP
+ HDOP
+ VDOP
+ Satelites en linea de vista
+ Fecha
+ COG(T)
+ COG(M)
+ Velocidad(km/h)

Por hacer
---------

Guardar los datos en archivo .csv
